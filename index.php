<?php 

$title = "Home"; 
include "pages/shared/header.php";

?>

<style>

body { 
  color: white; 
}

h1 {
  position: relative;
  z-index: 1;
  margin: 0 auto;
  width: 50%;
  background: rgba(0, 0, 0, 0.3);
  padding: 20px 0 20px 0;
  font-size: 40px;
}

footer {
  background: rgba(0, 0, 0, 0.3);
  padding: 3px 10px 10px 10px;
  bottom: 8px;
  color: white;
  left: 50%;  
  margin-left: -100px; 
}

</style>

  <body>

		<div id="wrapper">
  		<?php include "pages/shared/sidebar.php" ?>
  		<img class="bg" src="images/clinic.jpg">

        <!-- <div id="page-content-wrapper">
        </div> -->

    </div>
    <h1>Welcome to Brown Medical Clinic!</h1>

<footer>
      © Taylor. Last Updated 12/1/14
</footer>

<script src="<?php echo ROOT ?>/js/jquery.min.js"></script>
<script src="<?php echo ROOT ?>/js/bootstrap.min.js"></script>
<script src="<?php echo ROOT ?>/js/jquery-cookie.js"></script>

<script>

$(document).ready(function() {

  if (<?php echo $_SESSION['logged_in']? 'true' : 'false' ?>) {
    // if the user is logged in, set the toggle state
    // based on the cookie's saved state
    $("#wrapper").toggleClass($.cookie('toggle-state'));
  }
  else {
    // if the user is not logged in, hide the sidenav
    $("#wrapper").toggleClass("toggled");
    // if the cookie exists and they're not logged in, delete it
    if ($.cookie('toggle-state')) {
      $.removeCookie('toggle-state', {expires: 1, path: '/'});
    }
  }

  $("#menu-toggle").click(function(e) {
    e.preventDefault();

    // toggle the menu when the button is clicked
    $("#wrapper").toggleClass("toggled");

    // save state of toggle
    var new_value = $("#wrapper").hasClass("toggled") ? "toggled" : "";

    $.cookie('toggle-state', new_value, {expires: 1, path: '/'}); 
  });
});
</script>

  </body>

</html>
