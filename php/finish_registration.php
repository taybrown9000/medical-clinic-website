<?php 

$title = "Complete Registration"; 
include "../pages/shared/header.php";

if ($_POST) {

  // connect to the database
  require "mysqli_connect.php";
  $errors = array();

  // insert function
  if (isset($_POST['complete-btn'])) {

    // verify forms
    require "verify_patients.php";

    // if there were no errors
    if (empty($errors)) {

      // lock tables
      // $db->query("LOCK TABLES PATIENTS PEOPLE WRITE;");

      // set person_id and profile_id equal to patient_id
      $profile_id = $_SESSION['profile_id'];
      $person_id = $patient_id = $profile_id;

      // query to insert patient in patient table
      $sql = <<<SQL
      INSERT INTO PATIENTS (PERSON_ID, PROFILE_ID, INSURANCE_ID)
      VALUES ('$person_id','$profile_id', '$insurance_id');
SQL;
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      // query to insert patient in people table
      $sql = <<<SQL
      INSERT INTO PEOPLE (LAST_NAME, FIRST_NAME, 
      MIDDLE, DOB, SEX, ADDRESS, CITY, STATE, ZIPCODE, HOME_PHONE,
      CELL_PHONE)
      VALUES ('$last_name', '$first_name', '$middle', 
      '$DOB', '$sexRadio', '$address', '$city', '$state', '$zipcode', 
      '$home_phone', '$cell_phone');
SQL;
      if(!$result = $db->query($sql)) {
        die($db->error);
      }
    }
  } // End of insert function

  // unlock tables
  // $db->query("UNLOCK TABLES;");
  $db->close();

  $_SESSION['completed'] = true;
  unset($_SESSION['new_register']);
  header("Location: ../pages/dashboard.php");
}

?>

<body>

  <div id="wrapper">
  <?php include "../pages/shared/sidebar.php" ?>
  
    <div id="page-content-wrapper">
      <div class="container-fluid">

    <!-- Error/success messages -->
    <div class="col-md-6 col-md-offset-3">
    <?php

    // print errors here
    if (!empty($errors) && $_POST) {
      echo '
      <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error: </span>';
        foreach ($errors as $msg) {
          echo "<li>$msg</li>";
        }
      echo '</div>';
    }
    else {
      echo '<div class="alert alert-info" role="alert" 
            style="width: 60%; margin: 0 auto">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            Almost done! Please fill out the remaining fields to complete 
            your account registration.
          </div>';
    }

    ?>
    </div>

    <!-- Form -->
    <!-- Offset determined by (12 - (col-size) / 2) -->
    <div class="col-sm-12 col-md-6 col-md-offset-3">
      <form class="form-horizontal" role="form" method="post">

        <h1 class="form-heading">Patient Registration</h1><br />

        <!-- First Name -->
        <div class="form-group">
          <label for="fnameInput" class="col-sm-2 control-label">First Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" 
            placeholder="First Name" name="first_name" id="fnameInput"
            value="<?php echo $first_name ?>">
          </div>
        </div>

        <!-- Last Name -->
        <div class="form-group">
          <label for="lnameInput" class="col-sm-2 control-label">Last Name</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" 
            placeholder="Last Name" name="last_name" id="lnameInput"
            value="<?php echo $last_name ?>">
          </div>
        </div>

        <!-- Middle Initial -->
        <div class="form-group">
          <label for="middleInput" class="col-sm-2 control-label">
            Middle Init.
          </label>
          <div class="col-sm-10">
            <input type="text" class="form-control" 
            placeholder="Middle Initial" 
            name="middle" id="middleInput"
            value="<?php echo $middle ?>">
          </div>
        </div>

        <!-- DOB -->
        <div class="form-group">
          <label for="dobInput" class="col-sm-2 control-label">
            DOB
          </label>
          <div class="col-sm-10">
            <input type="text" class="form-control" 
            placeholder="YYYY-MM-DD" name="DOB" id="dobInput"
            value="<?php echo $DOB ?>">
          </div>
        </div>

        <!-- Sex -->
        <div class="form-group">
          <label class="col-sm-2 control-label">Sex</label>
          <div class="col-sm-2">
            <label class="radio-inline">
              <input type="radio" name="radioGroup" value="M" id="mRadio"
              <?php echo ($sexRadio == 'M')? 'checked':'' ?>> Male
            </label>
          </div>
          <div class="col-sm-2">
            <label class="radio-inline">
            <input type="radio" name="radioGroup" value="F" id="fRadio"
            <?php echo ($sexRadio == 'F')? 'checked':'' ?>> Female
            </label>
          </div>
        </div>

        <!-- Address -->
        <div class="form-group">
          <label for="addressInput" class="col-sm-2 control-label">
            Address
          </label>
          <div class="col-sm-10">
            <input type="text" class="form-control" 
            placeholder="Address" name="address" id="addressInput"
            value="<?php echo $address ?>">
          </div>
        </div>

        <!-- City -->
        <div class="form-group">
          <label for="cityInput" class="col-sm-2 control-label">
            City
          </label>
          <div class="col-sm-10">
            <input type="text" class="form-control" 
            placeholder="City" name="city" id="cityInput"
            value="<?php echo $city ?>">
          </div>
        </div>

        <!-- State -->
        <div class="form-group">
          <label for="stateInput" class="col-sm-2 control-label">
            State
          </label>
          <div class="col-sm-10">
            <select class="form-control" id="stateInput" name="state"> 
              <option value="AL">AL</option>
              <option value="AK">AK</option>
              <option value="AZ">AZ</option>
              <option value="AR">AR</option>
              <option value="CA">CA</option>
              <option value="CO">CO</option>
              <option value="CT">CT</option>
              <option value="DE">DE</option>
              <option value="DC">DC</option>
              <option value="FL">FL</option>
              <option value="GA">GA</option>
              <option value="HI">HI</option>
              <option value="ID">ID</option>
              <option value="IL">IL</option>
              <option value="IN">IN</option>
              <option value="IA">IA</option>
              <option value="KS">KS</option>
              <option value="KY">KY</option>
              <option value="LA">LA</option>
              <option value="ME">ME</option>
              <option value="MD">MD</option>
              <option value="MA">MA</option>
              <option value="MI">MI</option>
              <option value="MN">MN</option>
              <option value="MS">MS</option>
              <option value="MO">MO</option>
              <option value="MT">MT</option>
              <option value="NE">NE</option>
              <option value="NV">NV</option>
              <option value="NH">NH</option>
              <option value="NJ">NJ</option>
              <option value="NM">NM</option>
              <option value="NY">NY</option>
              <option value="NC">NC</option>
              <option value="ND">ND</option>
              <option value="OH">OH</option>
              <option value="OK">OK</option>
              <option value="OR">OR</option>
              <option value="PA">PA</option>
              <option value="RI">RI</option>
              <option value="SC">SC</option>
              <option value="SD">SD</option>
              <option value="TN">TN</option>
              <option value="TX">TX</option>
              <option value="UT">UT</option>
              <option value="VT">VT</option>
              <option value="VA">VA</option>
              <option value="WA">WA</option>
              <option value="WV">WV</option>
              <option value="WI">WI</option>
              <option value="WY">WY</option>
            </select>
          </div>
        </div>

        <!-- Zip Code -->
        <div class="form-group">
          <label for="zipcodeInput" class="col-sm-2 control-label">
            Zip Code
          </label>
          <div class="col-sm-10">
            <input type="number" class="form-control" 
            placeholder="Zip Code" name="zipcode" id="zipcodeInput"
            maxlength="5" value="<?php echo $zipcode ?>">
          </div>
        </div>

        <!-- Home Phone -->
        <div class="form-group">
          <label for="home_phoneInput" class="col-sm-2 control-label">
            Home Phone
          </label>
          <div class="col-sm-10">
            <input type="number" class="form-control" 
            placeholder="Home Phone" name="home_phone" 
            id="home_phoneInput" maxlength="10"
            value="<?php echo $home_phone ?>">
          </div>
        </div>

        <!-- Cell Phone -->
        <div class="form-group">
          <label for="cell_phoneInput" class="col-sm-2 control-label">
            Cell Phone
          </label>
          <div class="col-sm-10">
            <input type="number" class="form-control" 
            placeholder="Cell Phone" name="cell_phone" 
            id="cell_phoneInput" maxlength="10"
            value="<?php echo $cell_phone ?>">
          </div>
        </div>

        <!-- Insurance Companies -->
        <div class="form-group">
        <label for="insuranceInput" class="col-sm-2 control-label">
          Insurance Company
        </label>
          <div class="col-sm-10">
            <select class="form-control" id="insuranceInput" 
            name="insurance_company">
              <?php

              require "mysqli_connect.php";

              $sql = <<<SQL
              SELECT INSURANCE_ID, NAME
              FROM INSURANCE_COMPANIES;
SQL;
              if(!$result = $db->query($sql)) {
                die($db->error);
              }

              // get person and profile id
              while ($row = mysqli_fetch_array($result)) {
                $ins_id = $row['INSURANCE_ID'];
                $name = $row['NAME'];

                echo '<option value="'.$ins_id.'">'.$name.'</option>' 
                  . "\n";
              }

            ?>
            </select>
          </div>
        </div>
        
        <!-- Buttons -->
        <div class="form-group">
          <div class="row-fluid">

            <button type="submit" class="btn btn-block btn-primary btn-lg"
              name="complete-btn">
              Complete Registration
            </button>

          </div>
        </div>

      </form>
    </div><!-- End of form -->
  </div>
  </div>

  <?php include "../pages/shared/footer.php" ?>
  <script src="<?php echo ROOT ?>/js/bootstrapValidator.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.form-horizontal').bootstrapValidator();

        // setting correct values for selection inputs
        $("#stateInput option").filter(function() {
          return $(this).text() == <?php echo "'$state'" ?>;
        }).prop('selected', true);

        // setting correct values for selection inputs
        $("#insuranceInput option").filter(function() {
          return $(this).val() == <?php echo "'$insurance_id'" ?>;
        }).prop('selected', true);

      });
    </script>
</div>
</body>

</html>