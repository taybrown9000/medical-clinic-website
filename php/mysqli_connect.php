<?php

    // Establishes a connection to MySQL,
    // selects the database, and sets the encoding

    // Set the database access information as constants
    DEFINE ('DB_HOST', 'localhost');
    // // DEFINE ('DB_USER', 'TB6232');
    // // DEFINE ('DB_PASSWORD', 'rilley123');
    DEFINE ('DB_USER', 'root');
    DEFINE ('DB_PASSWORD', '');
    DEFINE ('DB_NAME', 'BROWN_CLINIC');

    // // Make the connection
    $db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) OR
                die('Could not connect to MySQL: ' . mysqli_connect_error());

    mysqli_set_charset($db, 'utf8');
?>