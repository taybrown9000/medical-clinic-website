<?php

    if (empty($_POST['name'])) {
      $errors[] = "The Company Name field is blank";
    } else {
      $name = mysqli_real_escape_string($db, trim($_POST['name']));
    }

    if (empty($_POST['address'])) {
      $errors[] = "The Address field is blank";
    } else {
      $address = mysqli_real_escape_string($db, trim($_POST['address']));
    }

    if (empty($_POST['city'])) {
      $errors[] = "The City field is blank";
    } else {
      $city = mysqli_real_escape_string($db, trim($_POST['city']));
    }

    $state = $_POST['state'];

    if (empty($_POST['zipcode'])) {
      $errors[] = "The Zip Code field is blank";
    } else {
      $zipcode = mysqli_real_escape_string($db, trim($_POST['zipcode']));
    }

?>