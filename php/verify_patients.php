<?php

    if (empty($_POST['first_name'])) {
      $errors[] = "The First Name field is blank";
    } else {
      $first_name = mysqli_real_escape_string($db, trim($_POST['first_name']));
    }

    if (empty($_POST['last_name'])) {
      $errors[] = "The Last Name field is blank";
    } else {
      $last_name = mysqli_real_escape_string($db, trim($_POST['last_name']));
    }

    if (empty($_POST['middle'])) {
      $errors[] = "The Middle Initial field is blank";
    } else {
      $middle = mysqli_real_escape_string($db, trim($_POST['middle']));
    }

    if (empty($_POST['DOB'])) {
      $errors[] = "The Date of Birth field is blank";
    } else {
      $DOB = mysqli_real_escape_string($db, trim($_POST['DOB']));
    }

    if (empty($_POST['radioGroup'])) {
      $errors[] = "A Sex wasn't selected";
    } else {
      $sexRadio = $_POST['radioGroup'];
    }

    if (empty($_POST['address'])) {
      $errors[] = "The Address field is blank";
    } else {
      $address = mysqli_real_escape_string($db, trim($_POST['address']));
    }

    if (empty($_POST['city'])) {
      $errors[] = "The City field is blank";
    } else {
      $city = mysqli_real_escape_string($db, trim($_POST['city']));
    }

    $state = $_POST['state'];

    if (empty($_POST['zipcode'])) {
      $errors[] = "The Zip Code field is blank";
    } else {
      $zipcode = mysqli_real_escape_string($db, trim($_POST['zipcode']));
    }

    if (empty($_POST['home_phone'])) {
      $errors[] = "The Home Phone field is blank";
    } else {
      $home_phone = mysqli_real_escape_string($db, trim($_POST['home_phone']));
    }

    if (empty($_POST['cell_phone'])) {
      $errors[] = "The Cell Phone field is blank";
    } else {
      $cell_phone = mysqli_real_escape_string($db, trim($_POST['cell_phone']));
    }

    if (!$_SESSION['new_register']) {

      if (empty($_POST['username'])) {
        $errors[] = "The Username field is blank";
      } else {
        $username = mysqli_real_escape_string($db, trim($_POST['username']));
      }

      if (empty($_POST['password'])) {
        $errors[] = "The Password field is blank";
      } else {
        $password = mysqli_real_escape_string($db, trim($_POST['password']));
      }

      if (empty($_POST['email'])) {
        $errors[] = "The Email field is blank";
      } else {
        $email = mysqli_real_escape_string($db, trim($_POST['email']));
      }
      
    }

    $insurance_id = $_POST['insurance_company'];

?>