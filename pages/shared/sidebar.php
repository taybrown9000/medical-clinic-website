<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            Menu
        </li>
        <li <?= echoActiveClass("dashboard") ?>>
            <a href="<?php echo ROOT ?>/pages/dashboard.php">User Dashboard</a>
        </li>
        <li <?= echoActiveClass("user_defined_reporting") ?>>
            <a href="<?php echo ROOT ?>/pages/user_defined_reporting.php">
                User-Defined Reporting
            </a>
        </li>

        <!-- show rest of list based on security level -->
        <?php

        // Security LVL 1
        if ($_SESSION['security_lvl'] == 1) {
        echo '
            <li ' , echoActiveClass("schedule_appt") , '>
                <a href="'.ROOT.'/pages/schedule_appt.php">Schedule an Appointment</a>
            </li>
            <li ' , echoActiveClass("download_forms") , '>
                <a href="'.ROOT.'/pages/download_forms.php">Download New Patient Form</a>
            </li>
            ';
        }

        // Security LVL 2
        else if ($_SESSION['security_lvl'] == 2) {
        echo '
            <li ' , echoActiveClass("billing") , '>
                <a href="'.ROOT.'/pages/billing.php">Billing</a>
            </li>
            <li ' , echoActiveClass("modify_employees") , '>
                <a href="'.ROOT.'/pages/modify_employees.php">Modify Employees</a>
            </li>
            <li ' , echoActiveClass("modify_patients") , '>
                <a href="'.ROOT.'/pages/modify_patients.php">Modify Patients</a>
            </li>
            <li ' , echoActiveClass("modify_insurance") , '>
                <a href="'.ROOT.'/pages/modify_insurance.php">Modify Insurance Companies</a>
            </li>
            <li ' , echoActiveClass("employee_listing") , '>
                <a href="'.ROOT.'/pages/employee_listing.php">View Employee Listing</a>
            </li>
            ';
        }

        // Security LVL 3
        else if ($_SESSION['security_lvl'] == 3) {
        echo '<li ' , echoActiveClass("modify_lab_services") , '>
                <a href="'.ROOT.'/pages/modify_lab_services.php">
                    Modify Lab Services
                </a>
            </li>
            <li ' , echoActiveClass("modify_xray_services") , '>
                <a href="'.ROOT.'/pages/modify_xray_services.php">
                    Modify X-Ray Services
                </a>
            </li>
            <li ' , echoActiveClass("modify_icd_codes") , '>
                <a href="'.ROOT.'/pages/modify_icd_codes.php">
                    Modify ICD-9 Codes
                </a>
            </li>';
        }

        // security LVL 4
        else if ($_SESSION['security_lvl'] == 4) {
        echo '
            <li ' , echoActiveClass("schedule_appt") , '>
                <a href="'.ROOT.'/pages/schedule_appt.php">Schedule an Appointment</a>
            </li>
            <li ' , echoActiveClass("download_forms") , '>
                <a href="'.ROOT.'/pages/download_forms.php">Download New Patient Form</a>
            </li>
            <li ' , echoActiveClass("billing") , '>
                <a href="'.ROOT.'/pages/billing.php">Billing</a>
            </li>
            <li ' , echoActiveClass("modify_employees") , '>
                <a href="'.ROOT.'/pages/modify_employees.php">Modify Employees</a>
            </li>
            <li ' , echoActiveClass("modify_patients") , '>
                <a href="'.ROOT.'/pages/modify_patients.php">Modify Patients</a>
            </li>
            <li ' , echoActiveClass("modify_insurance") , '>
                <a href="'.ROOT.'/pages/modify_insurance.php">Modify Insurance Companies</a>
            </li>
            <li ' , echoActiveClass("modify_lab_services") , '>
                <a href="'.ROOT.'/pages/modify_lab_services.php">
                    Modify Lab Services
                </a>
            </li>
            <li ' , echoActiveClass("modify_xray_services") , '>
                <a href="'.ROOT.'/pages/modify_xray_services.php">
                    Modify X-Ray Services
                </a>
            </li>
            <li ' , echoActiveClass("modify_icd_codes") , '>
                <a href="'.ROOT.'/pages/modify_icd_codes.php">
                    Modify ICD-9 Codes
                </a>
            </li>
            <li ' , echoActiveClass("employee_listing") , '>
                <a href="'.ROOT.'/pages/employee_listing.php">View Employees Listing</a>
            </li>
            ';
        }


        ?>
    </ul>
</div>

<div id="stickyDiv">
<?php

if ($_SESSION['logged_in']) {
    echo '<a id="menu-toggle" class="burger-menu" href="#"></a>';
}

?>
</div>

<div id="stickyAlias"></div>