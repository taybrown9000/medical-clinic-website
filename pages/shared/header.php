<?php 

session_start();

# for deployment
// define(ROOT, "http://acadweb1.salisbury.edu/~TB6232/clinic");
# for devel
define(ROOT, '');
define(UPLOAD_DIR, "../images/uploads/");

// ini_set('display_errors',1);
// error_reporting(E_ALL);

// require $_SERVER['DOCUMENT_ROOT']."/php/security_log.php";

function echoActiveClass($requestUri)
{
  $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

  if ($current_file_name == $requestUri)
    echo 'class="active"';
}

function upload_pic() {

  $myFile = $_FILES["picture"];

  if ($myFile["error"] !== UPLOAD_ERR_OK) {
      echo "<p>An error occurred.</p>";
      exit;
  }

  // ensure a safe filename
  $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

  // don't overwrite an existing file
  // $i = 0;
  // $parts = pathinfo($name);
  // while (file_exists(UPLOAD_DIR . $name)) {
  //     $i++;
  //     $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
  // }

  // preserve file from temporary directory
  $success = move_uploaded_file($myFile["tmp_name"],
      UPLOAD_DIR.$name);
  if (!$success) { 
      echo "<p>Unable to save file.</p>";
  }
}

?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="<?php echo ROOT ?>/favicon.ico">

  <title><?php echo "$title | Brown Medical Clinic" ?></title>

  <link href="<?php echo ROOT ?>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo ROOT ?>/css/navbar.css" rel="stylesheet">
  <link href="<?php echo ROOT ?>/css/simple-sidebar.css" rel="stylesheet">
  <link href="<?php echo ROOT ?>/css/styles.css" rel="stylesheet">

</head>

<header>
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="<?php echo ROOT ?>/index.php">
          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
          Brown Medical Clinic
        </a>
      </div>
      <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li <?= echoActiveClass("index") ?>>
            <a href="<?php echo ROOT ?>/index.php">Home</a></li>

          <!-- Dropdown -->
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" 
            role="button" aria-expanded="false">Services <span class="caret"></span></a>
            <ul class="dropdown-menu dropdown-menu-left" role="menu">
              <li <?= echoActiveClass("on-site") ?>>
                <a href="<?php echo ROOT ?>/pages/on-site.php">On-Site</a></li>
              <li <?= echoActiveClass("patient") ?>>
                <a href="<?php echo ROOT ?>/pages/patient.php">Patient</a></li>
              <li <?= echoActiveClass("occupational") ?>>
                <a href="<?php echo ROOT ?>/pages/occupational.php">Occupational</a></li>
            </ul>
          </li>

          <li <?= echoActiveClass("staff") ?>>
            <a href="<?php echo ROOT ?>/pages/staff.php">Staff</a></li>
          <li <?= echoActiveClass("about") ?>>
            <a href="<?php echo ROOT ?>/pages/about.php">About</a></li>

          <?php

          #if the user is logged in
          if ($_SESSION['logged_in']) {
            echo '
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" 
              role="button" aria-expanded="false">' , $_SESSION["username"] ,'
              <span class="caret"></span></a>
              <ul class="dropdown-menu dropdown-menu-left" role="menu">
                <li><a href="'. ROOT . '/pages/shared/logout.php">Logout</a></li>
              </ul>
            </li> 
            ';
          }
          else {
            echo '
              <li ' , echoActiveClass("login") , '>
              <a id="login" href="'. ROOT .'/pages/login.php">Log In</a></li>
              ';
          }

          ?>
          
          
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>
</header>