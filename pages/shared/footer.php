<footer>
	<div class="container-fluid">
			© Taylor. Last Updated 12/1/14
	</div>
</footer>

<script src="<?php echo ROOT ?>/js/jquery.min.js"></script>
<script src="<?php echo ROOT ?>/js/bootstrap.min.js"></script>
<script src="<?php echo ROOT ?>/js/jquery-cookie.js"></script>

<script>

$(document).ready(function() {

	if (<?php echo $_SESSION['logged_in']? 'true' : 'false' ?>) {
		// if the user is logged in, set the toggle state
		// based on the cookie's saved state
		$("#wrapper").toggleClass($.cookie('toggle-state'));
	}
	else {
		// if the user is not logged in, hide the sidenav
		$("#wrapper").toggleClass("toggled");
		// if the cookie exists and they're not logged in, delete it
		if ($.cookie('toggle-state')) {
			$.removeCookie('toggle-state', {expires: 1, path: '/'});
		}
	}

	$("#menu-toggle").click(function(e) {
		e.preventDefault();

		// toggle the menu when the button is clicked
		$("#wrapper").toggleClass("toggled");

		// save state of toggle
		var new_value = $("#wrapper").hasClass("toggled") ? "toggled" : "";

		$.cookie('toggle-state', new_value, {expires: 1, path: '/'}); 
	});

	$(function(){
    // Check the initial Poistion of the Sticky Div
    // var stickyDivTop = $('#stickyDiv').offset().top;
    var topOfPage = 0;

    $(window).scroll(function(){
    	if ( $(window).scrollTop() > topOfPage ) {
    		$('#stickyDiv').css({position: 'fixed', top: '50px'});
    		$('#stickyAlias').css('display', 'block');
    	} else {
    		$('#stickyDiv').css({position: 'static', top: '50px'});
    		$('#stickyAlias').css('display', 'none');
    	}
    });
  });

});

</script>
