<?php 
$title = "Dashboard"; 
include "shared/header.php";

if ($_SESSION['new_register']) {
  header("Location: ../php/finish_registration.php");
}

// get user info
require "../php/mysqli_connect.php";

$profile_id = $_SESSION['profile_id'];

// patient
if ($_SESSION['security_lvl'] == 1) {

  $sql = <<<SQL
  SELECT PERSON_ID
  FROM PATIENTS
  WHERE PROFILE_ID = '$profile_id';
SQL;
  if(!$result = $db->query($sql)) {
    die($db->error);
  }

  while ($row = mysqli_fetch_array($result)) {
      $person_id = $row['PERSON_ID'];
      $insurance_id = $row['INSURANCE_ID'];
    }

  $sql = <<<SQL
  SELECT NAME
  FROM INSURANCE_COMPANIES
  WHERE INSURANCE_ID = '$insurance_id';
SQL;
  if(!$result = $db->query($sql)) {
    die($db->error);
  }

  while ($row = mysqli_fetch_array($result)) {
      $insurance_name = $row['NAME'];
    }
}

// employee
else if ($_SESSION['security_lvl'] > 1) {
$sql = <<<SQL
    SELECT *
    FROM EMPLOYEES
    WHERE PROFILE_ID = '$profile_id';
SQL;
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    while ($row = mysqli_fetch_array($result)) {
        $person_id = $row['PERSON_ID'];
        $picture = $row['PICTURE'];
        $emp_type_num = $row['EMP_TYPE_NUM'];
    }

    $sql = <<<SQL
    SELECT TYPE
    FROM EMPLOYEE_TYPES
    WHERE EMP_TYPE_NUM = '$emp_type_num';
SQL;
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    while ($row = mysqli_fetch_array($result)) {
        $emp_type = $row['TYPE'];
    }
}

$sql = <<<SQL
    SELECT *
    FROM PEOPLE
    WHERE PERSON_ID = '$person_id';
SQL;

if(!$result = $db->query($sql)) {
    die($db->error);
  }

  while ($row = mysqli_fetch_array($result)) {
    $first_name = $row['FIRST_NAME'];
    $last_name = $row['LAST_NAME'];
    $middle = $row['MIDDLE'];
    $DOB = $row['DOB'];
    $sexRadio = $row['SEX'];
    $address = $row['ADDRESS'];
    $city = $row['CITY'];
    $state = $row['STATE'];
    $zipcode = $row['ZIPCODE'];
    $home_phone = $row['HOME_PHONE'];
    $cell_phone = $row['CELL_PHONE'];
  }

$sql = <<<SQL
    SELECT EMAIL
    FROM PROFILES
    WHERE PROFILE_ID = '$profile_id';
SQL;

if(!$result = $db->query($sql)) {
    die($db->error);
  }

  while ($row = mysqli_fetch_array($result)) {
    $email = $row['EMAIL'];
  }

?>

<body>

  <div id="wrapper">
    <?php include "shared/sidebar.php" ?>

    <div id="page-content-wrapper">
      <div class="container-fluid">
        <?php

        if ($_SESSION['new_login']) {
          echo '
          <div class="alert alert-success" role="alert" 
            style="width: 60%; margin: 0 auto">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
             Login successful! Welcome to your User Dashboard!
           </div>';
          unset($_SESSION['new_login']);
        } 

        elseif ($_SESSION['completed']) {
          echo '
          <div class="alert alert-success" role="alert" 
            style="width: 60%; margin: 0 auto">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              Registration Complete! Welcome to your User Dashboard!
           </div>';
        }
  
      ?>
      <div class="row">
        <h1>User Dashboard</h1>
      </div>

      <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
        <p class=" text-info" style="text-align: right"><?php echo date('D, M jS G:i A') ?></p>
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">
                <?php echo $first_name." ".$middle.". ".$last_name ?>
              </h3>
            </div>

            <div class="panel-body">
              <div class="row">

                <div class="col-md-3 col-lg-3 " align="center">
                <img alt="User Pic" 
                  src="<?php

                        if ($picture) { echo UPLOAD_DIR.$picture; }
                        else {
                          echo 'https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100';
                        }

                        ?>
                  " class="img-circle"> 

                </div>

                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information" style="text-align: left">
                    <tbody>
                      <tr>
                        <td>Profile Type</td>
                        <td>
                          <?php

                          if ($emp_type) {
                            echo $emp_type;
                          } else { echo "User"; }

                          ?>
                        </td>
                      </tr>
                      <tr>
                        <td>Security Level</td>
                        <td><?php echo $_SESSION['security_lvl'] ?></td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td><?php echo $DOB ?></td>
                      </tr>
                   
                      <tr>
                       <tr>
                        <td>Gender</td>
                        <td>
                          <?php
                          if ($sex == 'M') { echo "Male"; }
                          else { echo "Female"; } 
                          ?>
                        </td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td><?php echo $address.'<br />'.$city.', '.$state.' '.$zipcode ?></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:<?php echo $email?>"><?php echo $email ?></a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td><?php echo $home_phone ?> (Home)<br>
                          <?php echo $cell_phone ?> (Mobile)
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    </div>
  </div>
  <?php include "shared/footer.php" ?>
</div>
</body>

</html>
