<?php 

$title = "Modify ICD-9 Codes"; 
include "shared/header.php";

if ($_POST) {

  // connect to the database
  require "../php/mysqli_connect.php";
  $errors = array();

  // create variables
  $icd_id = mysqli_real_escape_string($db, trim($_POST['icd_id']));

  // retrieve function
  if (isset($_POST['retrieve-btn'])) {

    // lock tables
    $db->query("LOCK TABLES ICD_CODES READ;");

    // query to check if the code exists in the db
    $sql = <<<SQL
    SELECT *
    FROM ICD_CODES
    WHERE ICD_CODE = '$icd_id';
SQL;

    // see if there were any errors processing the query
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    // if the code doesn't exist in the database
    if ($result->num_rows == 0) {
      $errors[] = "That ICD-9 Code doesn't exist, cannot retrieve";
    }

    // if the code exists
    else if($result->num_rows == 1) {

      while ($row = mysqli_fetch_array($result)) {
        $icd_id = $row['ICD_CODE'];
        $description = $row['DESCRIPTION'];
      }
    }
  } // End of retrieve function

  // insert function
  elseif (isset($_POST['insert-btn'])) {

    // verify forms
    if (empty($_POST['description'])) {
      $errors[] = "The Description field is blank";
    } else {
      $description = mysqli_real_escape_string($db, 
        trim($_POST['description']));
    }

    // if there were no errors
    if (empty($errors)) {

      // lock tables
      $db->query("LOCK TABLES ICD_CODES WRITE;");

      // query to check if the code exists in the db
      $sql = <<<SQL
      SELECT *
      FROM ICD_CODES
      WHERE ICD_CODE = '$icd_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      // if the code exists in the database
      if ($result->num_rows >= 1) {
        $errors[] = "That ICD-9 Code already exists. 
          Please choose another";
      }
      // if the doesn't code exist yet
      else if($result->num_rows == 0) {

        // query to insert code in code table
        $sql = <<<SQL
        INSERT INTO ICD_CODES (ICD_CODE, DESCRIPTION)
        VALUES ('$icd_id', '$description');
SQL;
        if(!$result = $db->query($sql)) {
          die($db->error);
        }
      }
    }
  } // End of insert function

  // update function
  elseif (isset($_POST['update-btn'])) {

    // verify forms
    if (empty($_POST['description'])) {
      $errors[] = "The Description field is blank";
    } else {
      $description = mysqli_real_escape_string($db, 
        trim($_POST['description']));
    }

    // if there were no errors
    if (empty($errors)) {

      // lock tables
      $db->query("LOCK TABLES ICD_CODES WRITE;");

      // check if code does indeed exist
      $sql = <<<SQL
      SELECT *
      FROM ICD_CODES
      WHERE ICD_CODE = '$icd_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }
      // if the code doesn't exist in the database
      if ($result->num_rows == 0) {
        $errors[] = "That ICD-9 Code doesn't exist, cannot update";
      }

      // if the code exists, write to db
      else if($result->num_rows == 1) {

        // update code table
        $sql = <<<SQL
        UPDATE ICD_CODES
        SET ICD_Code='$icd_id', DESCRIPTION='$description'
        WHERE ICD_CODE = '$icd_id';
SQL;
        // see if there were any errors processing the query
        if(!$result = $db->query($sql)) {
          die($db->error);
        }
      }
    }
  } // End of update function

  if (isset($_POST['delete-btn'])) {

    // lock tables
    $db->query("LOCK TABLES ICD_CODES WRITE;");

    // query to check if the code exists in the db
    $sql = <<<SQL
    SELECT *
    FROM ICD_CODES
    WHERE ICD_CODE = '$icd_id';
SQL;

    // see if there were any errors processing the query
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    // if the code doesn't exist in the database
    if ($result->num_rows == 0) {
      $errors[] = "That ICD-9 Code doesn't exist, cannot delete";
    }

    // if the code exists
    else if($result->num_rows == 1) {

      // delete from codes table
      $sql = <<<SQL
      DELETE FROM ICD_CODES
      WHERE ICD_CODE = '$icd_id';
SQL;

      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      unset($icd_id);
    }
  } // End of delete function

  // unlock tables
  $db->query("UNLOCK TABLES;");
  $db->close();
}

?>

<body>

  <div id="wrapper">
  <?php include "shared/sidebar.php" ?>
  
    <div id="page-content-wrapper">
      <div class="container-fluid">

        <!-- Error/success messages -->
        <div class="col-md-6 col-md-offset-3">
        <?php

        // print errors here
        if (!empty($errors) && $_POST) {
          echo '
          <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error: </span>';
            foreach ($errors as $msg) {
              echo "<li>$msg</li>";
            }
          echo '</div>';
        }

        // print success message
        elseif (empty($errors) && $_POST) {
          echo '
          <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only">Error: </span>';
          
          // if the operation was retrieval
          if (isset($_POST['retrieve-btn'])) {
            echo "ICD-9 Service Found! The data for " .$icd_id." has been populated in the form below";
          }
          elseif (isset($_POST['insert-btn'])) {
            echo "Successfully inserted " .$icd_id." into the database!";
          }
          elseif (isset($_POST['update-btn'])) {
            echo "Successfully updated ".$icd_id."!";
          }
          elseif (isset($_POST['delete-btn'])) {
            echo "Successfully deleted that lab from the database!";
          }

          echo '</div>';
        }

        ?>
        </div>

        <!-- Form -->
        <!-- Offset determined by (12 - (col-size) / 2) -->
        <div class="col-sm-12 col-md-6 col-md-offset-3">
          <form class="form-horizontal" role="form" method="post">

            <h1 class="form-heading">Modify ICD-9 Codes</h1><br />

            <!-- ICD-9 Code -->
            <div class="form-group">
              <label for="icd_idInput" class="col-sm-2 control-label">
                ICD-9 Code
              </label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="number" class="form-control" 
                  placeholder="ICD-9 Code" name="icd_id" 
                  min="1" maxlength="3" id="icd_idInput" 
                  value="<?php echo $icd_id ?>"
                  required autofocus>
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-primary" 
                    type="submit" name="retrieve-btn">
                      Retrieve
                    </button>
                  </span>
                </div>
              </div>
            </div>

            <!-- Description -->
            <div class="form-group">
              <label for="descInput" class="col-sm-2 control-label">
                Description
                </label>
              <div class="col-sm-10">
                <textarea type="textarea" class="form-control" 
                placeholder="Description" name="description" 
                id="descInput"><?php echo $description ?></textarea>
              </div>
            </div>

            <!-- Buttons -->
            <div class="form-group">
              <div class="row-fluid">

                <div class="col-sm-3">
                  <button type="submit" class="btn btn-success btn-lg"
                    name="insert-btn">
                    Insert
                  </button>
                </div>

                  <div class="col-sm-3">
                    <button type="submit" class="btn btn-info btn-lg"
                      name="update-btn">
                      Update
                    </button>
                  </div>

                  <div class="col-sm-3">
                    <button type="submit" class="btn btn-danger btn-lg"
                      name="delete-btn"
                      onclick="return confirm('Are you sure you want to delete?')">
                      Delete
                    </button>
                  </div>

                  <div class="col-sm-3">
                    <button type="button" class="btn btn-warning btn-lg"
                      id="reset">
                      Reset
                    </button>
                  </div>
               
                </div>
            </div>

          </form>
        </div><!-- End of form -->

      </div>
    </div>

    <?php include "shared/footer.php" ?>
    <script src="<?php echo ROOT ?>/js/bootstrapValidator.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.form-horizontal').bootstrapValidator();

        // reset button
        $("#reset").click(function() {
          $(this).closest('form').find("input, textarea").val("");
          $('.alert').hide();
        });
      });
    </script>

  </div>
</body>

</html>
