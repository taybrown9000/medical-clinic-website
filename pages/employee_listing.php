<?php 
$title = "Employee Listing"; 
include "shared/header.php";

require "../php/mysqli_connect.php";

?>

  <body>

  	<div id="wrapper">
  		<?php include "shared/sidebar.php" ?>

      <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="row">
          <h1>Employee Listing</h1>
        </div>

          <div class="container-fluid">
              
          <table class="table table-striped">

            <thead>
              <tr>
                <th>Employee ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle</th>
                <th>DOB</th>
                <th>Address</th>
                <th>City</th>
                <th>State</th>
                <th>Zip Code</th>
                <th>Home Phone</th>
                <th>Cell Phone</th>
                <th>Employee Type</th>
                <th>Username</th>
                <th>Password</th>
                <th>Email</th>
                <th>Security LVL</th>
                <th>Picture</th>
              </tr>
            </thead>

            <tbody>
              <?php 

              $db->query("LOCK TABLES EMPLOYEES PEOPLE PROFILES READ;");

              $sql = <<<SQL
              SELECT *
              FROM EMPLOYEES E, PROFILES P, PEOPLE PE, 
              EMPLOYEE_TYPES ET
              WHERE E.PROFILE_ID = P.PROFILE_ID AND
              E.PERSON_ID = PE.PERSON_ID AND
              E.EMP_TYPE_NUM = ET.EMP_TYPE_NUM;
SQL;
              if(!$result = $db->query($sql)) {
                die($db->error);
              }

              // store variables we'll need for later
              while ($row = mysqli_fetch_array($result)) {

                echo "<tr>";
                echo "<td>".$row['EMPLOYEE_ID']."</td>";
                echo "<td>".$row['FIRST_NAME']."</td>";
                echo "<td>".$row['LAST_NAME']."</td>";
                echo "<td>".$row['MIDDLE']."</td>";
                echo "<td>".$row['DOB']."</td>";
                echo "<td>".$row['ADDRESS']."</td>";
                echo "<td>".$row['CITY']."</td>";
                echo "<td>".$row['STATE']."</td>";
                echo "<td>".$row['ZIPCODE']."</td>";
                echo "<td>".$row['HOME_PHONE']."</td>";
                echo "<td>".$row['CELL_PHONE']."</td>";
                echo "<td>".$row['TYPE']."</td>";
                echo "<td>".$row['USERNAME']."</td>";
                echo "<td>".$row['PASSWORD']."</td>";
                echo "<td>".$row['EMAIL']."</td>";
                echo "<td>".$row['SECURITY_LVL']."</td>";
                echo "<td><img src=".UPLOAD_DIR.$row['PICTURE']."></td>";
                echo "</tr>";
              }

          $db->close();
          ?>

        </tbody>

      </table>

    </div>

  			</div>
        </div>
       <?php include "shared/footer.php" ?>
      </div>
  </body>

</html>
