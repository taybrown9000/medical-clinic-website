<?php 

$title = "Sign Up"; 
include "shared/header.php";

if ($_POST) {

  # connect to the database
  require "../php/mysqli_connect.php";

	# create variables
  $username = mysqli_real_escape_string($db, trim($_POST['username']));
  $password = mysqli_real_escape_string($db, trim($_POST['password']));
  $email = mysqli_real_escape_string($db, trim($_POST['email']));

  # lock tables
  $db->query("LOCK TABLES PROFILES WRITE;");

	# query to check if the user already exists in the db
	$sql = <<<SQL
 		SELECT *
 		FROM PROFILES
 		WHERE USERNAME LIKE '$username';
SQL;

	# see if there were any errors processing the query
	if(!$result = $db->query($sql)) {
    die($db->error);
	}

	# if the user already exists in the database
	if ($result->num_rows > 0) {
    $already_exists = true;
  }
  # if the user does not have an account yet
  else if($result->num_rows == 0) {

    # store user in database
    $sql = <<<SQL
      INSERT INTO PROFILES
      (SECURITY_LVL, USERNAME, PASSWORD, EMAIL)
      VALUES (1, '$username', '$password', '$email');
SQL;

    # see if there were any errors processing the query
    if(!$result = $db->query($sql)) {
      die($db->error);
    }
    $id = $db->insert_id;

    # store session variables
    $_SESSION['username'] = $username;
    $_SESSION['security_lvl'] = 1;
    $_SESSION['profile_id'] = $id;
    $_SESSION['new_register'] = true;
    $_SESSION['logged_in'] = true;

    # unlock tables
    $db->query("UNLOCK TABLES;");
    $db->close();
    header("Location: dashboard.php");
  }
}

?>

  <body>

  <?php

  # if the user already exists in the database, flash error message
  if ($already_exists) {
    echo '
      <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        Username already exists in the database. Please choose a different username.
      </div>
    ';
  }

  ?>

  	<div class="container centered">
  		
      <form class="form-signin" role="form" method="post"
        data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
        data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
        data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">

  			<h1 class="form-signin-heading">Create a new account</h1>

        <div class="form-group">
    			<label for="inputUser" class="sr-only">Username</label>
    			<input type="text" id="inputUser" class="form-control" placeholder="Username" name="username" required autofocus>
        </div>

        <div class="form-group">
          <label for="pass" class="sr-only">Password</label>
          <input type="password" id="pass" class="form-control" 
          name="password" placeholder="Password" required
          data-bv-identical="true"
          data-bv-identical-field="confirmPassword"
          data-bv-identical-message="Passwords do not match" />
        </div>

        <div class="form-group">
          <label for="confirmPass" class="sr-only">Retype password</label>
          <input type="password" id="confirmPass" 
          class="form-control" name="confirmPassword" placeholder="Retype Password" required
          data-bv-identical="true"
          data-bv-identical-field="password"
          data-bv-identical-message="Passwords do not match" />
        </div>

        <div class="form-group">
          <label for="email" class="sr-only">Email</label>
          <input type="email" id="email" class="form-control" 
          name="email" placeholder="Email" required />
        </div>

  			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
  		</form>

  	</div>

    <?php include "shared/footer.php" ?>
    <script src="<?php echo ROOT ?>/js/bootstrapValidator.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.form-signin').bootstrapValidator();
      });
    </script>
  </body>

</html>
