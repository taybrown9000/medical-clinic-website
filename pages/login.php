<?php 

$title = "Log In"; 
include "shared/header.php";

if ($_POST) {

  # connect to the database
  require "../php/mysqli_connect.php";

	# create variables
	$username = mysqli_real_escape_string($db, trim($_POST['username']));
	$password = mysqli_real_escape_string($db, trim($_POST['password']));

  #lock tables
  $db->query("LOCK TABLES PROFILES READ;");

	$sql = <<<SQL
		SELECT SECURITY_LVL, PROFILE_ID
		FROM PROFILES
		WHERE USERNAME LIKE '$username' AND
		PASSWORD LIKE '$password';
SQL;

	# see if there were any errors processing the query
	if(!$result = $db->query($sql)) {
    die($db->error);
	}

	# if the user was found in the database
	if ($result->num_rows == 1) {

    # get session variables
    while ($row = mysqli_fetch_array($result)) {
      $_SESSION['security_lvl'] = $row['SECURITY_LVL'];
      $_SESSION['profile_id'] = $row['PROFILE_ID'];
    }

    $_SESSION['username'] = $username;
		$_SESSION['logged_in'] = true;
    $_SESSION['new_login'] = true;

  }
  $db->query("UNLOCK TABLES;");
  $db->close();

  header("Location: dashboard.php");
}

?>

  <body>

  <?php

  # flash error message if the login info was invalid
  if ($_POST && !$_SESSION['logged_in']) {
  	echo '
			<div class="alert alert-danger" role="alert">
			  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			  <span class="sr-only">Error:</span>
			  Incorrect username/password combination
			</div>
		';
  }

  ?>

  	<div class="container centered">
  		
  		<form class="form-signin" role="form" method="post">
  			<h1 class="form-signin-heading">Welcome!</h1>
  			<label for="inputUser" class="sr-only">Username</label>
  			<input type="text" id="inputUser" class="form-control" 
          placeholder="Username" name="username" required autofocus>
  			<label for="inputPassword" class="sr-only">Password</label>
  			<input type="password" id="inputPassword" class="form-control" 
          placeholder="Password" name="password" required>
  			<br />
  			<button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
  		</form>

  		Don't have an account? <a href="register.php">Register now!</a>
  		
  	</div>

    <?php include "shared/footer.php" ?>
  </body>

</html>
