<?php 

$title = "Modify Lab Services"; 
include "shared/header.php";

if ($_POST) {

  // connect to the database
  require "../php/mysqli_connect.php";
  $errors = array();

  // create variables
  $lab_id = mysqli_real_escape_string($db, trim($_POST['lab_id']));

  // retrieve function
  if (isset($_POST['retrieve-btn'])) {

    // lock tables
    $db->query("LOCK TABLES LAB_CODES READ;");

    // query to check if the lab exists in the db
    $sql = <<<SQL
    SELECT *
    FROM LAB_CODES
    WHERE LAB_ID = '$lab_id';
SQL;

    // see if there were any errors processing the query
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    // if the lab doesn't exist in the database
    if ($result->num_rows == 0) {
      $errors[] = "That Lab ID doesn't exist, cannot retrieve";
    }

    // if the lab exists
    else if($result->num_rows == 1) {

      // store variables we'll need for later
      while ($row = mysqli_fetch_array($result)) {
        $lab_id = $row['LAB_ID'];
        $service_name = $row['SERVICE_NAME'];
        $fee = $row['FEE'];
      }
    }
  } // End of retrieve function

  // insert function
  elseif (isset($_POST['insert-btn'])) {

    // verify forms
    if (empty($_POST['service_name'])) {
      $errors[] = "The Service Name field is blank";
    } else {
      $service_name = mysqli_real_escape_string($db, 
        trim($_POST['service_name']));
    }

    if (empty($_POST['fee'])) {
      $errors[] = "The Fee field is blank";
    } else {
      $fee = mysqli_real_escape_string($db, trim($_POST['fee']));
    }

    // if there were no errors
    if (empty($errors)) {

      // lock tables
      $db->query("LOCK TABLES LAB_CODES WRITE;");

      // query to check if the lab exists in the db
      $sql = <<<SQL
      SELECT *
      FROM LAB_CODES
      WHERE LAB_ID = '$lab_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      // if the lab exists in the database
      if ($result->num_rows >= 1) {
        $errors[] = "That Lab ID already exists. 
          Please choose another";
      }
      // if the doesn't lab exist yet
      else if($result->num_rows == 0) {

        // query to insert lab in lab table
        $sql = <<<SQL
        INSERT INTO LAB_CODES (LAB_ID, SERVICE_NAME, FEE)
        VALUES ('$lab_id', '$service_name', '$fee');
SQL;
        if(!$result = $db->query($sql)) {
          die($db->error);
        }
      }
    }
  } // End of insert function

  // update function
  elseif (isset($_POST['update-btn'])) {

    // verify forms
    if (empty($_POST['service_name'])) {
      $errors[] = "The Service Name field is blank";
    } else {
      $service_name = mysqli_real_escape_string($db, 
        trim($_POST['service_name']));
    }

    if (empty($_POST['fee'])) {
      $errors[] = "The Fee field is blank";
    } else {
      $fee = mysqli_real_escape_string($db, trim($_POST['fee']));
    }

    // if there were no errors
    if (empty($errors)) {

      // lock tables
      $db->query("LOCK TABLES LAB_CODES WRITE;");

      // check if lab does indeed exist
      $sql = <<<SQL
      SELECT *
      FROM LAB_CODES
      WHERE LAB_ID = '$lab_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }
      // if the lab doesn't exist in the database
      if ($result->num_rows == 0) {
        $errors[] = "That Lab ID doesn't exist, cannot update";
      }

      // if the lab exists, write to db
      else if($result->num_rows == 1) {

        // update insurance table
        $sql = <<<SQL
        UPDATE LAB_CODES
        SET LAB_ID='$lab_id', SERVICE_NAME='$service_name',
        FEE='$fee'
        WHERE LAB_ID = '$lab_id';
SQL;
        // see if there were any errors processing the query
        if(!$result = $db->query($sql)) {
          die($db->error);
        }
      }
    }
  } // End of update function

  if (isset($_POST['delete-btn'])) {

    // lock tables
    $db->query("LOCK TABLES LAB_CODES WRITE;");

    // query to check if the lab exists in the db
    $sql = <<<SQL
    SELECT *
    FROM LAB_CODES
    WHERE LAB_ID = '$lab_id';
SQL;

    // see if there were any errors processing the query
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    // if the lab doesn't exist in the database
    if ($result->num_rows == 0) {
      $errors[] = "That Lab ID doesn't exist, cannot delete";
    }

    // if the lab exists
    else if($result->num_rows == 1) {

      // delete from labs table
      $sql = <<<SQL
      DELETE FROM LAB_CODES
      WHERE LAB_ID = '$lab_id';
SQL;

      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      unset($lab_id);
    }
  } // End of delete function

  // unlock tables
  $db->query("UNLOCK TABLES;");
  $db->close();
}

?>

<body>

  <div id="wrapper">
  <?php include "shared/sidebar.php" ?>
  
    <div id="page-content-wrapper">
      <div class="container-fluid">

        <!-- Error/success messages -->
        <div class="col-md-6 col-md-offset-3">
        <?php

        // print errors here
        if (!empty($errors) && $_POST) {
          echo '
          <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error: </span>';
            foreach ($errors as $msg) {
              echo "<li>$msg</li>";
            }
          echo '</div>';
        }

        // print success message
        elseif (empty($errors) && $_POST) {
          echo '
          <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only">Error: </span>';
          
          // if the operation was retrieval
          if (isset($_POST['retrieve-btn'])) {
            echo "Lab Service Found! The data for " .$service_name." has been populated in the form below";
          }
          elseif (isset($_POST['insert-btn'])) {
            echo "Successfully inserted " .$service_name." into the database!";
          }
          elseif (isset($_POST['update-btn'])) {
            echo "Successfully updated ".$service_name."!";
          }
          elseif (isset($_POST['delete-btn'])) {
            echo "Successfully deleted that lab from the database!";
          }

          echo '</div>';
        }

        ?>
        </div>

        <!-- Form -->
        <!-- Offset determined by (12 - (col-size) / 2) -->
        <div class="col-sm-12 col-md-6 col-md-offset-3">
          <form class="form-horizontal" role="form" method="post">

            <h1 class="form-heading">Modify Lab Services</h1><br />

            <!-- Lab ID -->
            <div class="form-group">
              <label for="lab_idInput" class="col-sm-2 control-label">
                Lab ID
              </label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="number" class="form-control" 
                  placeholder="Lab ID" name="lab_id" 
                  min="1" maxlength="3" id="lab_idInput" 
                  value="<?php echo $lab_id ?>"
                  required autofocus>
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-primary" 
                    type="submit" name="retrieve-btn">
                      Retrieve
                    </button>
                  </span>
                </div>
              </div>
            </div>

            <!-- Service Name -->
            <div class="form-group">
              <label for="serviceNameInput" class="col-sm-2 control-label">
                Service Name
                </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="Service Name" name="service_name" id="serviceNameInput"
                value="<?php echo $service_name ?>">
              </div>
            </div>

            <!-- Fee -->
            <div class="form-group">
              <label for="feeInput" class="col-sm-2 control-label">
                Fee
              </label>
              <div class="col-sm-10">
                <input type="number" class="form-control" 
                placeholder="Cost" name="fee" id="feeInput"
                min="1" value="<?php echo $fee ?>">
              </div>
            </div>

            <!-- Buttons -->
            <div class="form-group">
              <div class="row-fluid">

                <div class="col-sm-3">
                  <button type="submit" class="btn btn-success btn-lg"
                    name="insert-btn">
                    Insert
                  </button>
                </div>

                  <div class="col-sm-3">
                    <button type="submit" class="btn btn-info btn-lg"
                      name="update-btn">
                      Update
                    </button>
                  </div>

                  <div class="col-sm-3">
                    <button type="submit" class="btn btn-danger btn-lg"
                      name="delete-btn"
                      onclick="return confirm('Are you sure you want to delete?')">
                      Delete
                    </button>
                  </div>

                  <div class="col-sm-3">
                    <button type="button" class="btn btn-warning btn-lg"
                      id="reset">
                      Reset
                    </button>
                  </div>
               
                </div>
            </div>

          </form>
        </div><!-- End of form -->

      </div>
    </div>

    <?php include "shared/footer.php" ?>
    <script src="<?php echo ROOT ?>/js/bootstrapValidator.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.form-horizontal').bootstrapValidator();

        // reset button
        $("#reset").click(function() {
          $(this).closest('form').find("input").val("");
          $('.alert').hide();
        });
      });
    </script>

  </div>
</body>

</html>
