<?php 

$title = "Modify Employees"; 
include "shared/header.php";

if ($_POST) {

  // connect to the database
  require "../php/mysqli_connect.php";
  $errors = array();

  // create variables
  $emp_id = mysqli_real_escape_string($db, trim($_POST['emp_id']));

  // retrieve function
  if (isset($_POST['retrieve-btn'])) {

    // lock tables
    $db->query("LOCK TABLES EMPLOYEES PEOPLE PROFILES READ;");

    // query to check if the employee exists in the db
    $sql = <<<SQL
    SELECT *
    FROM EMPLOYEES
    WHERE EMPLOYEE_ID = '$emp_id';
SQL;

    // see if there were any errors processing the query
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    // if the employee doesn't exist in the database
    if ($result->num_rows == 0) {
      $errors[] = "That Employee ID doesn't exist, cannot retrieve";
    }

    // if the employee exists
    else if($result->num_rows == 1) {

      // store variables we'll need for later
      while ($row = mysqli_fetch_array($result)) {
        $person_id = $row['PERSON_ID'];
        $profile_id = $row['PROFILE_ID'];
        $emp_type_num = $row['EMP_TYPE_NUM'];
        $picture = $row['PICTURE'];
      }

      // query to get person info
      $sql = <<<SQL
      SELECT *
      FROM PEOPLE
      WHERE PERSON_ID = '$person_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      // store the rest of the variables for form
      while ($row = mysqli_fetch_array($result)) {
        $first_name = $row['FIRST_NAME'];
        $last_name = $row['LAST_NAME'];
        $middle = $row['MIDDLE'];
        $DOB = $row['DOB'];
        $sexRadio = $row['SEX'];
        $address = $row['ADDRESS'];
        $city = $row['CITY'];
        $state = $row['STATE'];
        $zipcode = $row['ZIPCODE'];
        $home_phone = $row['HOME_PHONE'];
        $cell_phone = $row['CELL_PHONE'];
      }

      // query to get profile info
      $sql = <<<SQL
      SELECT *
      FROM PROFILES
      WHERE PROFILE_ID = '$profile_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      // store the rest of the variables for form
      while ($row = mysqli_fetch_array($result)) {
        $security_lvl = $row['SECURITY_LVL'];
        $username = $row['USERNAME'];
        $password = $row['PASSWORD'];
        $email = $row['EMAIL'];
      }
    }
  } // End of retrieve function

  // insert function
  elseif (isset($_POST['insert-btn'])) {

    // verify forms
    require "../php/verify_employees.php";

    // if there were no errors
    if (empty($errors)) {

      // lock tables
      $db->query("LOCK TABLES EMPLOYEES PEOPLE PROFILES WRITE;");

      // query to check if the employee exists in the db
      $sql = <<<SQL
      SELECT EMP_TYPE_NUM, PERSON_ID
      FROM EMPLOYEES
      WHERE EMPLOYEE_ID = '$emp_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      // if the employee exists in the database
      if ($result->num_rows >= 1) {
        $errors[] = "That Employee ID already exists. 
          Please choose another";
      }
      // if the doesn't employee exist yet
      else if($result->num_rows == 0) {

        // set person_id and profile_id equal to emp_id
        $person_id = $profile_id = $emp_id;

        // check if username already exists
        $sql = <<<SQL
        SELECT USERNAME
        FROM PROFILES
        WHERE USERNAME LIKE '$username';
SQL;

        if(!$result = $db->query($sql)) {
          die($db->error);
        }

        if ($result->num_rows > 0) {
          $errors[] = "That Username already exists. 
          Please choose another";
        }
        elseif ($result->num_rows == 0) {

          // query to insert employee in employee table
          $sql = <<<SQL
          INSERT INTO EMPLOYEES (EMPLOYEE_ID, PERSON_ID, PROFILE_ID, 
          EMP_TYPE_NUM, PICTURE)
          VALUES ('$emp_id', '$person_id', '$profile_id', 
          '$emp_type_num', '$picture');
SQL;
          if(!$result = $db->query($sql)) {
            die($db->error);
          }

          upload_pic();

          // query to insert employee in people table
          $sql = <<<SQL
          INSERT INTO PEOPLE (PERSON_ID, LAST_NAME, FIRST_NAME, 
          MIDDLE, DOB, SEX, ADDRESS, CITY, STATE, ZIPCODE, HOME_PHONE,
          CELL_PHONE)
          VALUES ('$person_id', '$last_name', '$first_name', '$middle', 
          '$DOB', '$sexRadio', '$address', '$city', '$state', '$zipcode', 
          '$home_phone', '$cell_phone');
SQL;
          if(!$result = $db->query($sql)) {
            die($db->error);
          }

          // query to insert employee in profiles table
          $sql = <<<SQL
          INSERT INTO PROFILES (PROFILE_ID, SECURITY_LVL, USERNAME, 
          PASSWORD, EMAIL)
          VALUES ('$profile_id', '$security_lvl', '$username', '$password', 
          '$email');
SQL;
          if(!$result = $db->query($sql)) {
            die($db->error);
          }
        }
      }
    }
  } // End of insert function

  // update function
  elseif (isset($_POST['update-btn'])) {

    // verify forms
    require "../php/verify_employees.php";

    // if there were no errors
    if (empty($errors)) {

      // lock tables
      $db->query("LOCK TABLES EMPLOYEES PROFILES PEOPLE WRITE;");

      // check if employee does indeed exist
      $sql = <<<SQL
      SELECT PERSON_ID, PROFILE_ID
      FROM EMPLOYEES
      WHERE EMPLOYEE_ID = '$emp_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }
      // if the employee doesn't exist in the database
      if ($result->num_rows == 0) {
        $errors[] = "That Employee ID doesn't exist, cannot update";
      }

      // if the employee exists, write to db
      else if($result->num_rows == 1) {

        // check if username already exists
        $sql = <<<SQL
        SELECT USERNAME
        FROM PROFILES
        WHERE USERNAME LIKE '$username';
SQL;

        if(!$result = $db->query($sql)) {
          die($db->error);
        }

        if ($result->num_rows > 1) {
          $errors[] = "That Username already exists. 
          Please choose another";
        }

        elseif ($result->num_rows == 1) {

          // get person and profile id
          while ($row = mysqli_fetch_array($result)) {
            $person_id = $row['PERSON_ID'];
            $profile_id = $row['PROFILE_ID'];
          }

          // update people table
          $sql = <<<SQL
          UPDATE PEOPLE
          SET LAST_NAME='$last_name', FIRST_NAME='$first_name',
          MIDDLE='$middle', DOB='$DOB', SEX='$sexRadio', ADDRESS='$address',
          CITY='$city', STATE='$state', ZIPCODE='$zipcode',
          HOME_PHONE='$home_phone', CELL_PHONE='$cell_phone'
          WHERE PERSON_ID = '$person_id';
SQL;
          // see if there were any errors processing the query
          if(!$result = $db->query($sql)) {
            die($db->error);
          }

          // update profiles table
          $sql = <<<SQL
          UPDATE PROFILES
          SET SECURITY_LVL='$security_lvl', USERNAME='$username',
          PASSWORD='$password', EMAIL='$email'
          WHERE PROFILE_ID = '$profile_id';
SQL;
          // see if there were any errors processing the query
          if(!$result = $db->query($sql)) {
            die($db->error);
          }

          // get current picture
          $sql = <<<SQL
          SELECT PICTURE
          FROM EMPLOYEES
          WHERE EMPLOYEE_ID = '$emp_id';
SQL;
          if(!$result = $db->query($sql)) {
            die($db->error);
          }

          while ($row = mysqli_fetch_array($result)) {
            $picture = $row['PICTURE'];
          }

          // if the user selected a new picture to upload
          if ($_FILES["picture"]["name"] != '') {
            // // upload new pic
            upload_pic();
            // // delete old picture
            unlink(UPLOAD_DIR . $picture);
            // // set picture to new filename
            $picture = $_FILES["picture"]["name"];
          }

          // update employees table
          $sql = <<<SQL
          UPDATE EMPLOYEES
          SET EMP_TYPE_NUM='$emp_type_num', 
          PICTURE='$picture'
          WHERE EMPLOYEE_ID = '$emp_id';
SQL;
          // see if there were any errors processing the query
          if(!$result = $db->query($sql)) {
            die($db->error);
          }
        }
      }
    }
  } // End of update function

  if (isset($_POST['delete-btn'])) {

    // lock tables
    $db->query("LOCK TABLES EMPLOYEES PROFILES PEOPLE WRITE;");

    // query to check if the employee exists in the db
    $sql = <<<SQL
    SELECT *
    FROM EMPLOYEES
    WHERE EMPLOYEE_ID = '$emp_id';
SQL;

    // see if there were any errors processing the query
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    // if the employee doesn't exist in the database
    if ($result->num_rows == 0) {
      $errors[] = "That Employee ID doesn't exist, cannot delete";
    }

    // if the employee exists
    else if($result->num_rows == 1) {

      // store variables we'll need for later
      while ($row = mysqli_fetch_array($result)) {
        $person_id = $row['PERSON_ID'];
        $profile_id = $row['PROFILE_ID'];
        $emp_type_num = $row['EMP_TYPE_NUM'];
        $picture = $row['PICTURE'];
      }

      // delete from employees table
      $sql = <<<SQL
      DELETE FROM EMPLOYEES
      WHERE EMPLOYEE_ID = '$emp_id';
SQL;

      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      // delete picture locally
      unlink(UPLOAD_DIR . $picture);

      // delete from people table
      $sql = <<<SQL
      DELETE FROM PEOPLE
      WHERE PERSON_ID = '$person_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      // delete from profiles table
      $sql = <<<SQL
      DELETE FROM PROFILES
      WHERE PROFILE_ID = '$profile_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      unset($emp_id);
    }
  } // End of delete function

  // unlock tables
  $db->query("UNLOCK TABLES;");
  $db->close();
}

?>

<body>

  <div id="wrapper">
  <?php include "shared/sidebar.php" ?>
  
    <div id="page-content-wrapper">
      <div class="container-fluid">

        <!-- Error/success messages -->
        <div class="col-md-6 col-md-offset-3">
        <?php

        // print errors here
        if (!empty($errors) && $_POST) {
          echo '
          <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error: </span>';
            foreach ($errors as $msg) {
              echo "<li>$msg</li>";
            }
          echo '</div>';
        }

        // print success message
        elseif (empty($errors) && $_POST) {
          echo '
          <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only">Error: </span>';
          
          // if the operation was retrieval
          if (isset($_POST['retrieve-btn'])) {
            echo "Employee Found! " .$first_name." ".$last_name."'s data has been populated in the form below";
          }
          elseif (isset($_POST['insert-btn'])) {
            echo "Successfully inserted " .$first_name." ".$last_name." into the database!";
          }
          elseif (isset($_POST['update-btn'])) {
            echo "Successfully updated ".$first_name." ".$last_name."!";
          }
          elseif (isset($_POST['delete-btn'])) {
            echo "Successfully deleted that employee from the database!";
          }

          echo '</div>';
        }

        ?>
        </div>

        <!-- Form -->
        <!-- Offset determined by (12 - (col-size) / 2) -->
        <div class="col-sm-12 col-md-6 col-md-offset-3">
          <form class="form-horizontal" role="form" method="post"
            enctype="multipart/form-data">

            <h1 class="form-heading">Modify Employee Info</h1><br />

            <!-- Employee Number -->
            <div class="form-group">
              <label for="emp_idInput" class="col-sm-2 control-label">Employee ID</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="number" class="form-control" 
                  placeholder="Employee ID" name="emp_id" 
                  min="1" maxlength="3" id="emp_idInput" 
                  value="<?php echo $emp_id ?>"
                  required autofocus>
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-primary" 
                    type="submit" name="retrieve-btn">
                      Retrieve
                    </button>
                  </span>
                </div>
              </div>
            </div>

            <!-- First Name -->
            <div class="form-group">
              <label for="fnameInput" class="col-sm-2 control-label">First Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="First Name" name="first_name" id="fnameInput"
                value="<?php echo $first_name ?>">
              </div>
            </div>

            <!-- Last Name -->
            <div class="form-group">
              <label for="lnameInput" class="col-sm-2 control-label">Last Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="Last Name" name="last_name" id="lnameInput"
                value="<?php echo $last_name ?>">
              </div>
            </div>

            <!-- Middle Initial -->
            <div class="form-group">
              <label for="middleInput" class="col-sm-2 control-label">
                Middle Init.
              </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="Middle Initial" 
                name="middle" id="middleInput"
                value="<?php echo $middle ?>">
              </div>
            </div>

            <!-- DOB -->
            <div class="form-group">
              <label for="dobInput" class="col-sm-2 control-label">
                DOB
              </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="YYYY-MM-DD" name="DOB" id="dobInput"
                value="<?php echo $DOB ?>">
              </div>
            </div>

            <!-- Sex -->
            <div class="form-group">
              <label class="col-sm-2 control-label">Sex</label>
              <div class="col-sm-2">
                <label class="radio-inline">
                  <input type="radio" name="radioGroup" value="M" id="mRadio"
                  <?php echo ($sexRadio == 'M')? 'checked':'' ?>> Male
                </label>
              </div>
              <div class="col-sm-2">
                <label class="radio-inline">
                <input type="radio" name="radioGroup" value="F" id="fRadio"
                <?php echo ($sexRadio == 'F')? 'checked':'' ?>> Female
                </label>
              </div>
            </div>

            <!-- Address -->
            <div class="form-group">
              <label for="addressInput" class="col-sm-2 control-label">
                Address
              </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="Address" name="address" id="addressInput"
                value="<?php echo $address ?>">
              </div>
            </div>

            <!-- City -->
            <div class="form-group">
              <label for="cityInput" class="col-sm-2 control-label">
                City
              </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="City" name="city" id="cityInput"
                value="<?php echo $city ?>">
              </div>
            </div>

            <!-- State -->
            <div class="form-group">
            <label for="stateInput" class="col-sm-2 control-label">
              State
            </label>
              <div class="col-sm-10">
                <select class="form-control" id="stateInput" name="state"> 
                  <option value="AL">AL</option>
                  <option value="AK">AK</option>
                  <option value="AZ">AZ</option>
                  <option value="AR">AR</option>
                  <option value="CA">CA</option>
                  <option value="CO">CO</option>
                  <option value="CT">CT</option>
                  <option value="DE">DE</option>
                  <option value="DC">DC</option>
                  <option value="FL">FL</option>
                  <option value="GA">GA</option>
                  <option value="HI">HI</option>
                  <option value="ID">ID</option>
                  <option value="IL">IL</option>
                  <option value="IN">IN</option>
                  <option value="IA">IA</option>
                  <option value="KS">KS</option>
                  <option value="KY">KY</option>
                  <option value="LA">LA</option>
                  <option value="ME">ME</option>
                  <option value="MD">MD</option>
                  <option value="MA">MA</option>
                  <option value="MI">MI</option>
                  <option value="MN">MN</option>
                  <option value="MS">MS</option>
                  <option value="MO">MO</option>
                  <option value="MT">MT</option>
                  <option value="NE">NE</option>
                  <option value="NV">NV</option>
                  <option value="NH">NH</option>
                  <option value="NJ">NJ</option>
                  <option value="NM">NM</option>
                  <option value="NY">NY</option>
                  <option value="NC">NC</option>
                  <option value="ND">ND</option>
                  <option value="OH">OH</option>
                  <option value="OK">OK</option>
                  <option value="OR">OR</option>
                  <option value="PA">PA</option>
                  <option value="RI">RI</option>
                  <option value="SC">SC</option>
                  <option value="SD">SD</option>
                  <option value="TN">TN</option>
                  <option value="TX">TX</option>
                  <option value="UT">UT</option>
                  <option value="VT">VT</option>
                  <option value="VA">VA</option>
                  <option value="WA">WA</option>
                  <option value="WV">WV</option>
                  <option value="WI">WI</option>
                  <option value="WY">WY</option>
                </select>
              </div>
            </div>

            <!-- Zip Code -->
            <div class="form-group">
              <label for="zipcodeInput" class="col-sm-2 control-label">
                Zip Code
              </label>
              <div class="col-sm-10">
                <input type="number" class="form-control" 
                placeholder="Zip Code" name="zipcode" id="zipcodeInput"
                maxlength="5" value="<?php echo $zipcode ?>">
              </div>
            </div>

            <!-- Home Phone -->
            <div class="form-group">
              <label for="home_phoneInput" class="col-sm-2 control-label">
                Home Phone
              </label>
              <div class="col-sm-10">
                <input type="number" class="form-control" 
                placeholder="Home Phone" name="home_phone" 
                id="home_phoneInput" maxlength="10"
                value="<?php echo $home_phone ?>">
              </div>
            </div>

            <!-- Cell Phone -->
            <div class="form-group">
              <label for="cell_phoneInput" class="col-sm-2 control-label">
                Cell Phone
              </label>
              <div class="col-sm-10">
                <input type="number" class="form-control" 
                placeholder="Cell Phone" name="cell_phone" 
                id="cell_phoneInput" maxlength="10"
                value="<?php echo $cell_phone ?>">
              </div>
            </div>

            <!-- Employee Type -->
            <div class="form-group">
              <label for="emp_typeInput" class="col-sm-2 control-label">
              Employee Type
              </label>
              <div class="col-sm-10">
                <select class="form-control" id="emp_typeInput" 
                name="emp_type_num">
                  <option value="1" selected="selected">Physician</option>
                  <option value="2">Secretary</option>
                  <option value="3">Lab Technician</option>
                  <option value="4">X-Ray Technician</option>
                </select>
              </div>
            </div>

            <!-- Username -->
            <div class="form-group">
              <label for="usernameInput" class="col-sm-2 control-label">
                Username
              </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="Username" name="username" id="usernameInput"
                value="<?php echo $username ?>">
              </div>
            </div>

            <!-- Password -->
            <div class="form-group">
              <label for="passwordInput" class="col-sm-2 control-label">
                Password
              </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="Password" name="password" id="passwordInput"
                value="<?php echo $password ?>">
              </div>
            </div>

            <!-- Email -->
            <div class="form-group">
              <label for="emailInput" class="col-sm-2 control-label">
                Email
              </label>
              <div class="col-sm-10">
                <input type="email" class="form-control" 
                placeholder="Email" name="email" id="emailInput"
                value="<?php echo $email ?>">
              </div>
            </div>

            <!-- Security LVL -->
            <div class="form-group">
              <label for="security_lvlInput" class="col-sm-2 control-label">
              Security Level
              </label>
              <div class="col-sm-10">
                <select class="form-control" id="security_lvlInput"
                  name="security_lvl">
                  <option value="1" selected="selected">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
              </div>
            </div>

            <!-- Profile Picture -->
            <div class="form-group">
              <label for="picInput" class="col-sm-2 control-label">
                Employee Picture
              </label>
              <div class="col-sm-2">
                <input type="file" name="picture" id="picInput">
              </div>
              <div class="col-sm-8">
                <?php

                if (!isset($_POST['delete-btn']) and $_POST) {
                  echo '<img src="'.UPLOAD_DIR.$picture.'">';
                  echo '<p>'.$picture.'</p>';
                }

                ?>
              </div>
            </div>
            
            <!-- Buttons -->
            <div class="form-group">
              <div class="row-fluid">

                <div class="col-sm-3">
                  <button type="submit" class="btn btn-success btn-lg"
                    name="insert-btn">
                    Insert
                  </button>
                </div>

                  <div class="col-sm-3">
                    <button type="submit" class="btn btn-info btn-lg"
                      name="update-btn">
                      Update
                    </button>
                  </div>

                  <div class="col-sm-3">
                    <button type="submit" class="btn btn-danger btn-lg"
                      name="delete-btn"
                      onclick="return confirm('Are you sure you want to delete?')">
                      Delete
                    </button>
                  </div>

                  <div class="col-sm-3">
                    <button type="button" class="btn btn-warning btn-lg"
                      id="reset">
                      Reset
                    </button>
                  </div>
               
                </div>
            </div>

          </form>
        </div><!-- End of form -->

      </div>
    </div>

    <?php include "shared/footer.php" ?>
    <script src="<?php echo ROOT ?>/js/bootstrapValidator.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.form-horizontal').bootstrapValidator();

        // setting correct values for selection inputs
        $("#stateInput option").filter(function() {
          return $(this).text() == <?php echo "'$state'" ?>;
        }).prop('selected', true);

        $("#emp_typeInput option").filter(function() {
          return $(this).val() == <?php echo "'$emp_typeInput'" ?>;
        }).prop('selected', true);

        $("#security_lvlInput option").filter(function() {
          return $(this).val() == <?php echo "'$security_lvl'" ?>;
        }).prop('selected', true);

        // reset button
        $("#reset").click(function() {
          $(this).closest('form').find("input").val("");
          $('input:radio').prop('checked',false);
          $('select').prop('selectedIndex',0);
          $('.alert').hide();
        });
      });
    </script>

  </div>
</body>

</html>
