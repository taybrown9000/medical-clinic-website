<?php 

$title = "Modify Insurance Companies"; 
include "shared/header.php";

if ($_POST) {

  // connect to the database
  require "../php/mysqli_connect.php";
  $errors = array();

  // create variables
  $insurance_id = mysqli_real_escape_string($db, trim($_POST['insurance_id']));

  // retrieve function
  if (isset($_POST['retrieve-btn'])) {

    // lock tables
    $db->query("LOCK TABLES INSURANCE_COMPANIES READ;");

    // query to check if the company exists in the db
    $sql = <<<SQL
    SELECT *
    FROM INSURANCE_COMPANIES
    WHERE INSURANCE_ID = '$insurance_id';
SQL;

    // see if there were any errors processing the query
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    // if the company doesn't exist in the database
    if ($result->num_rows == 0) {
      $errors[] = "That Company ID doesn't exist, cannot retrieve";
    }

    // if the company exists
    else if($result->num_rows == 1) {

      // store variables we'll need for later
      while ($row = mysqli_fetch_array($result)) {
        $insurance_id = $row['INSURANCE_ID'];
        $name = $row['NAME'];
        $address = $row['ADDRESS'];
        $city = $row['CITY'];
        $state = $row['STATE'];
        $zipcode = $row['ZIPCODE'];
      }
    }
  } // End of retrieve function

  // insert function
  elseif (isset($_POST['insert-btn'])) {

    // verify forms
    require "../php/verify_insurance.php";

    // if there were no errors
    if (empty($errors)) {

      // lock tables
      $db->query("LOCK TABLES INSURANCE_COMPANIES WRITE;");

      // query to check if the company exists in the db
      $sql = <<<SQL
      SELECT *
      FROM INSURANCE_COMPANIES
      WHERE INSURANCE_ID = '$insurance_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      // if the company exists in the database
      if ($result->num_rows >= 1) {
        $errors[] = "That Company ID already exists. 
          Please choose another";
      }
      // if the doesn't company exist yet
      else if($result->num_rows == 0) {

        // query to insert company in company table
        $sql = <<<SQL
        INSERT INTO INSURANCE_COMPANIES (INSURANCE_ID, NAME, ADDRESS, 
        CITY, STATE, ZIPCODE)
        VALUES ('$insurance_id', '$name', '$address', '$city', 
        '$state', '$zipcode');
SQL;
        if(!$result = $db->query($sql)) {
          die($db->error);
        }
      }
    }
  } // End of insert function

  // update function
  elseif (isset($_POST['update-btn'])) {

    // verify forms
    require "../php/verify_insurance.php";

    // if there were no errors
    if (empty($errors)) {

      // lock tables
      $db->query("LOCK TABLES INSURANCE_COMPANIES WRITE;");

      // check if company does indeed exist
      $sql = <<<SQL
      SELECT *
      FROM INSURANCE_COMPANIES
      WHERE INSURANCE_ID = '$insurance_id';
SQL;
      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }
      // if the company doesn't exist in the database
      if ($result->num_rows == 0) {
        $errors[] = "That Company ID doesn't exist, cannot update";
      }

      // if the company exists, write to db
      else if($result->num_rows == 1) {

        // update insurance table
        $sql = <<<SQL
        UPDATE INSURANCE_COMPANIES
        SET INSURANCE_ID='$insurance_id', NAME='$name',
        ADDRESS='$address', CITY='$city', STATE='$state', 
        ZIPCODE='$zipcode'
        WHERE INSURANCE_ID = '$insurance_id';
SQL;
        // see if there were any errors processing the query
        if(!$result = $db->query($sql)) {
          die($db->error);
        }
      }
    }
  } // End of update function

  if (isset($_POST['delete-btn'])) {

    // lock tables
    $db->query("LOCK TABLES INSURANCE_COMPANIES WRITE;");

    // query to check if the company exists in the db
    $sql = <<<SQL
    SELECT *
    FROM INSURANCE_COMPANIES
    WHERE INSURANCE_ID = '$insurance_id';
SQL;

    // see if there were any errors processing the query
    if(!$result = $db->query($sql)) {
      die($db->error);
    }

    // if the company doesn't exist in the database
    if ($result->num_rows == 0) {
      $errors[] = "That Company ID doesn't exist, cannot delete";
    }

    // if the company exists
    else if($result->num_rows == 1) {

      // delete from companys table
      $sql = <<<SQL
      DELETE FROM INSURANCE_COMPANIES
      WHERE INSURANCE_ID = '$insurance_id';
SQL;

      // see if there were any errors processing the query
      if(!$result = $db->query($sql)) {
        die($db->error);
      }

      unset($insurance_id);
    }
  } // End of delete function

  // unlock tables
  $db->query("UNLOCK TABLES;");
  $db->close();
}

?>

<body>

  <div id="wrapper">
  <?php include "shared/sidebar.php" ?>
  
    <div id="page-content-wrapper">
      <div class="container-fluid">

        <!-- Error/success messages -->
        <div class="col-md-6 col-md-offset-3">
        <?php

        // print errors here
        if (!empty($errors) && $_POST) {
          echo '
          <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error: </span>';
            foreach ($errors as $msg) {
              echo "<li>$msg</li>";
            }
          echo '</div>';
        }

        // print success message
        elseif (empty($errors) && $_POST) {
          echo '
          <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only">Error: </span>';
          
          // if the operation was retrieval
          if (isset($_POST['retrieve-btn'])) {
            echo "Insurance Company Found! The data for " .$name." has been populated in the form below";
          }
          elseif (isset($_POST['insert-btn'])) {
            echo "Successfully inserted " .$name." into the database!";
          }
          elseif (isset($_POST['update-btn'])) {
            echo "Successfully updated ".$name."!";
          }
          elseif (isset($_POST['delete-btn'])) {
            echo "Successfully deleted that company from the database!";
          }

          echo '</div>';
        }

        ?>
        </div>

        <!-- Form -->
        <!-- Offset determined by (12 - (col-size) / 2) -->
        <div class="col-sm-12 col-md-6 col-md-offset-3">
          <form class="form-horizontal" role="form" method="post">

            <h1 class="form-heading">Modify Insurance Company Info</h1><br />

            <!-- Insurance Company Number -->
            <div class="form-group">
              <label for="insurance_idInput" class="col-sm-2 control-label">Company ID</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="number" class="form-control" 
                  placeholder="Company ID" name="insurance_id" 
                  min="1" maxlength="3" id="insurance_idInput" 
                  value="<?php echo $insurance_id ?>"
                  required autofocus>
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-primary" 
                    type="submit" name="retrieve-btn">
                      Retrieve
                    </button>
                  </span>
                </div>
              </div>
            </div>

            <!-- Name -->
            <div class="form-group">
              <label for="cnameInput" class="col-sm-2 control-label">
                Company Name
                </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="Company Name" name="name" id="cnameInput"
                value="<?php echo $name ?>">
              </div>
            </div>

            <!-- Address -->
            <div class="form-group">
              <label for="addressInput" class="col-sm-2 control-label">
                Address
              </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="Address" name="address" id="addressInput"
                value="<?php echo $address ?>">
              </div>
            </div>

            <!-- City -->
            <div class="form-group">
              <label for="cityInput" class="col-sm-2 control-label">
                City
              </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" 
                placeholder="City" name="city" id="cityInput"
                value="<?php echo $city ?>">
              </div>
            </div>

            <!-- State -->
            <div class="form-group">
            <label for="stateInput" class="col-sm-2 control-label">
              State
            </label>
              <div class="col-sm-10">
                <select class="form-control" id="stateInput" name="state"> 
                  <option value="AL">AL</option>
                  <option value="AK">AK</option>
                  <option value="AZ">AZ</option>
                  <option value="AR">AR</option>
                  <option value="CA">CA</option>
                  <option value="CO">CO</option>
                  <option value="CT">CT</option>
                  <option value="DE">DE</option>
                  <option value="DC">DC</option>
                  <option value="FL">FL</option>
                  <option value="GA">GA</option>
                  <option value="HI">HI</option>
                  <option value="ID">ID</option>
                  <option value="IL">IL</option>
                  <option value="IN">IN</option>
                  <option value="IA">IA</option>
                  <option value="KS">KS</option>
                  <option value="KY">KY</option>
                  <option value="LA">LA</option>
                  <option value="ME">ME</option>
                  <option value="MD">MD</option>
                  <option value="MA">MA</option>
                  <option value="MI">MI</option>
                  <option value="MN">MN</option>
                  <option value="MS">MS</option>
                  <option value="MO">MO</option>
                  <option value="MT">MT</option>
                  <option value="NE">NE</option>
                  <option value="NV">NV</option>
                  <option value="NH">NH</option>
                  <option value="NJ">NJ</option>
                  <option value="NM">NM</option>
                  <option value="NY">NY</option>
                  <option value="NC">NC</option>
                  <option value="ND">ND</option>
                  <option value="OH">OH</option>
                  <option value="OK">OK</option>
                  <option value="OR">OR</option>
                  <option value="PA">PA</option>
                  <option value="RI">RI</option>
                  <option value="SC">SC</option>
                  <option value="SD">SD</option>
                  <option value="TN">TN</option>
                  <option value="TX">TX</option>
                  <option value="UT">UT</option>
                  <option value="VT">VT</option>
                  <option value="VA">VA</option>
                  <option value="WA">WA</option>
                  <option value="WV">WV</option>
                  <option value="WI">WI</option>
                  <option value="WY">WY</option>
                </select>
              </div>
            </div>

            <!-- Zip Code -->
            <div class="form-group">
              <label for="zipcodeInput" class="col-sm-2 control-label">
                Zip Code
              </label>
              <div class="col-sm-10">
                <input type="number" class="form-control" 
                placeholder="Zip Code" name="zipcode" id="zipcodeInput"
                maxlength="5" value="<?php echo $zipcode ?>">
              </div>
            </div>

            <!-- Buttons -->
            <div class="form-group">
              <div class="row-fluid">

                <div class="col-sm-3">
                  <button type="submit" class="btn btn-success btn-lg"
                    name="insert-btn">
                    Insert
                  </button>
                </div>

                  <div class="col-sm-3">
                    <button type="submit" class="btn btn-info btn-lg"
                      name="update-btn">
                      Update
                    </button>
                  </div>

                  <div class="col-sm-3">
                    <button type="submit" class="btn btn-danger btn-lg"
                      name="delete-btn"
                      onclick="return confirm('Are you sure you want to delete?')">
                      Delete
                    </button>
                  </div>

                  <div class="col-sm-3">
                    <button type="button" class="btn btn-warning btn-lg"
                      id="reset">
                      Reset
                    </button>
                  </div>
               
                </div>
            </div>

          </form>
        </div><!-- End of form -->

      </div>
    </div>

    <?php include "shared/footer.php" ?>
    <script src="<?php echo ROOT ?>/js/bootstrapValidator.min.js"></script>
    <script>
      $(document).ready(function() {
        $('.form-horizontal').bootstrapValidator();

        // setting correct values for selection inputs
        $("#stateInput option").filter(function() {
          return $(this).text() == <?php echo "'$state'" ?>;
        }).prop('selected', true);

        // reset button
        $("#reset").click(function() {
          $(this).closest('form').find("input").val("");
          $('input:radio').prop('checked',false);
          $('select').prop('selectedIndex',0);
          $('.alert').hide();
        });
      });
    </script>

  </div>
</body>

</html>
